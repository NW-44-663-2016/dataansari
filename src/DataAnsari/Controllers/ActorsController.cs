using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataAnsari.Models;

namespace DataAnsari.Controllers
{
    public class ActorsController : Controller
    {
        private AppDbContext _context;

        public ActorsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Actors
        public IActionResult Index()
        {
            var appDbContext = _context.Actors.Include(a => a.Location);
            return View(appDbContext.ToList());
        }

        // GET: Actors/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Actor actor = _context.Actors.Single(m => m.ActorID == id);
            if (actor == null)
            {
                return HttpNotFound();
            }

            return View(actor);
        }

        // GET: Actors/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: Actors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Actor actor)
        {
            if (ModelState.IsValid)
            {
                _context.Actors.Add(actor);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", actor.LocationID);
            return View(actor);
        }

        // GET: Actors/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Actor actor = _context.Actors.Single(m => m.ActorID == id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", actor.LocationID);
            return View(actor);
        }

        // POST: Actors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Actor actor)
        {
            if (ModelState.IsValid)
            {
                _context.Update(actor);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", actor.LocationID);
            return View(actor);
        }

        // GET: Actors/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Actor actor = _context.Actors.Single(m => m.ActorID == id);
            if (actor == null)
            {
                return HttpNotFound();
            }

            return View(actor);
        }

        // POST: Actors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Actor actor = _context.Actors.Single(m => m.ActorID == id);
            _context.Actors.Remove(actor);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
